# Running the code
## Requirements
The following non-standard packages were used in our code:
- matplotlib
- numpy
- numba
- tqdm
- skopt
- scipy
- pandas
- seaborn

These can be installed by running the following code

`pip install matplotlib`  
`pip install numpy`  
`pip install numba`  
`pip install tqdm`  
`pip install scikit-optimize`  
`pip install scipy`  
`pip install pandas`  
`pip install seaborn`  

## Running the code
The implementations of the algorithm and the simulations have been done in jupyter notebooks.
The notebook Mandelbrot-plotting-finalsim-stattest.ipynb contains the statistical tests, running of final simulations and initial plotting of the set.

The notebook Convergence-of-estimate-area.ipynb contains the analysis of the convergence of the simulations, the influence of i and s and the confidence intervals.


Note that some data used for generating the figures is already saved in the Data folder.
Figures are saved in the Figures folder. The cells of code in the notebook that are used to generate this code are commented out, to allow quick running of the code for generating the plots. It is mentioned when this is the case. Uncommenting and running these cells will mean new data generation and overwriting of the existing data, which might take some time to run.
